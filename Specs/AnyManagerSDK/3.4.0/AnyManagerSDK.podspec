Pod::Spec.new do |s|
    s.name              = 'AnyManagerSDK'

    s.version           = '3.4.0'

    s.summary           = 'AnySDK'

    s.homepage          = 'https://github.com/AnyMindG/AnyManagerSDK.git'

    s.author            = { 'Name' => 'plugindev@pokkt.com' }

    s.license           = { :type => 'Apache', :file => 'LICENSE' }

    s.platform          = :ios

    s.source            = { :http => 'https://github.com/AnyMindG/AnyManagerSDK/releases/download/6000.9.0/AnyManagerSDK.zip'}

    s.vendored_frameworks = 'AnyManagerSDK/AnyManagerSDK.xcframework'

    s.resource_bundles = {"AnyManagerSDK_PrivacyInfo" => ["AnyManagerSDK/AnyManagerSDK.xcframework/ios-arm64/AnyManagerSDK.framework/*.{xcprivacy}"]}

    s.source_files = 'AnyManagerSDK/*.{swift}'

    s.ios.deployment_target = '13'

    s.swift_version = ['3', '5', '5.7']

    s.static_framework = true
    
    s.frameworks = "CoreData", "MediaPlayer", "Foundation", "UIKit", "CoreTelephony", "AdSupport", "CoreGraphics", "CoreMotion", "MessageUI", "EventKit", "EventKitUI", "CoreLocation", "AVFoundation", "CFNetwork", "StoreKit", "WebKit", "PassKit" , "AudioToolbox" , "CoreMedia" , "JavaScriptCore" , "CoreServices" , "SafariServices" , "Social" , "AppTrackingTransparency" , "MobileCoreServices"

    s.libraries = "xml2.2", "c++", "xml2" , "z" , "sqlite3" , "z.1.2.5"

    s.ios.framework   = 'SystemConfiguration'

    s.dependency "GoogleMobileAdsMediationAppLovin"
    s.dependency "GoogleMobileAdsMediationInMobi"
    s.dependency "GoogleMobileAdsMediationVungle"
    s.dependency "GoogleMobileAdsMediationFacebook", '6.15.0'
    s.dependency "GoogleMobileAdsMediationMintegral"
    s.dependency "GoogleMobileAdsMediationPangle"
    s.dependency "Smart-Display-SDK"
    s.dependency "GoogleMobileAdsMediationFyber"
    s.dependency "GoogleMobileAdsMediationChartboost"
    s.dependency "GoogleMobileAdsMediationIronSource"
    s.dependency "GoogleMobileAdsMediationUnity"

end





