Pod::Spec.new do |s|
    s.name              = 'AnyManagerMediation'

    s.version           = '2.2.0'

    s.summary           = 'AnyManagerMediation'

    s.homepage          = 'https://github.com/AnyMindG/AnyManagerGAM.git'

    s.author            = { 'Name' => 'plugindev@pokkt.com' }

    s.license           = { :type => 'Apache', :file => 'LICENSE' }

    s.platform          = :ios

    s.source            = { :http => 'https://github.com/AnyMindG/AnyManagerGAM/releases/download/2.2.0/AnyManagerMediation.zip'}

    s.source_files =  'AnyManagerMediation/*.{h,m}'

    s.static_framework = true

    s.ios.deployment_target = '11.0'

    s.dependency "Google-Mobile-Ads-SDK"
        
end