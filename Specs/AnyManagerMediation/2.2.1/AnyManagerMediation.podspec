Pod::Spec.new do |s|
    s.name              = 'AnyManagerMediation'

    s.version           = '2.2.1'

    s.summary           = 'AnyManagerMediation'

    s.homepage          = 'https://github.com/AnyMindG/AnyManagerGAM.git'

    s.author            = { 'Name' => 'plugindev@pokkt.com' }

    s.license           = { :type => 'Apache', :file => 'LICENSE' }

    s.platform          = :ios

    s.source            = { :http => 'https://github.com/AnyMindG/AnyManagerMediation/releases/download/2.2.1/AnyManagerMediation.zip'}

    s.vendored_frameworks = 'AnyManagerMediation/AnyManagerMediation.xcframework'

    s.resource_bundles = {"AnyManagerMediation_PrivacyInfo" => ["AnyManagerMediation/AnyManagerMediation.xcframework/ios-arm64/AnyManagerMediation.framework/*.{xcprivacy}"]}

    s.ios.deployment_target = '13'

    s.swift_version = ['3', '5', '5.7']

    s.static_framework = true

    s.dependency "Google-Mobile-Ads-SDK"
        
end